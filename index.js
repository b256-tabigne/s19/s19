/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/


/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.
*/
console.log("Hello World");

let username
let password
let role


const login = function() {

    username = prompt("Insert Username: ");
    password = prompt("Insert Password: ");
    role = prompt("Insert Role(Teacher, Admin, Student): ");

    if(username == ""){
        alert("Input must not be empty.")
    }

    if(password == ""){
        alert("Input must not be empty.")
    }

    if(role == ""){
        alert("Role out of range.")
    }

    else if(role == "Teacher"){
        alert("Thank you for logging in, teacher!")
    }

    else if(role == "Admin"){
        alert("Welcome back to the class portal, admin!")
    }

    else if(role == "Student"){
        alert("Welcome to the class portal, student!")
    }

    else{
        alert("Role out of range.")
    }
}

login()


// CHECK AVERAGE


const average = function(g1,g2,g3,g4) {

    let checkaverage = (g1 + g2 + g3 + g4) / 4

    if(Math.round(checkaverage) <= 74){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is F`)
    }else if(Math.round(checkaverage) >= 75 && Math.round(checkaverage) <= 79){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is D`)
    }else if(Math.round(checkaverage) >= 80 && Math.round(checkaverage) <= 84){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is C`)
    }else if(Math.round(checkaverage) >= 85 && Math.round(checkaverage) <= 89){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is B`)
    }else if(Math.round(checkaverage) >= 90 && Math.round(checkaverage) <= 95){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is A`)
    }else if(Math.round(checkaverage) >= 96){
        console.log(`-Hello student your average  is: ${Math.round(checkaverage)}. The letter equivalent is A+`)
    }
}


average(71,70,73,74);
average(75,75,76,78);
average(80,81,82,78);
average(84,85,87,88);
average(89,90,91,90);
average(95,96,97,98);

/*let totalave = average(71,70,73,74)
console.log(totalave)

let ave1 = "checkAverage(75,75,76,78)";
console.log(ave1);
let totalave1 = average(75,75,76,78)
console.log(totalave1)


let ave2 = "checkAverage(80,81,82,78)";
console.log(ave2);
let totalave2 = average(80,81,82,78)
console.log(totalave2)


let ave3 = "checkAverage(84,85,87,88)";
console.log(ave3);
let totalave3 = average(84,85,87,88)
console.log(totalave3)


let ave4 = "checkAverage(89,90,91,90)";
console.log(ave4);
let totalave4 = average(89,90,91,90)
console.log(totalave4)


let ave5 = "checkAverage(95,96,97,98)";
console.log(ave5);
let totalave5 = average(95,96,97,98)
console.log(totalave5)*/